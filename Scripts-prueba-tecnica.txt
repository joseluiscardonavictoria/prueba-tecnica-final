//Script creación tabla
CREATE DATABASE prueba
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;


//Dominio
INSERT INTO public.tb_parameters(
	parameter_id, name_parameter, value_parameter)
	VALUES (1, 'DOMINIO', '@cidenet.com');