package com.app.pruebatecnicaback.enums;

public enum Gender {
    Masculino, Femenino, Otro
}
