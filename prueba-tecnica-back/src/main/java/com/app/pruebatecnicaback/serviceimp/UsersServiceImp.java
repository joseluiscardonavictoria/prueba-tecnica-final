package com.app.pruebatecnicaback.serviceimp;

import com.app.pruebatecnicaback.dto.UsersDTO;
import com.app.pruebatecnicaback.entity.Rol;
import com.app.pruebatecnicaback.entity.Users;
import com.app.pruebatecnicaback.mapping.UsersMapping;
import com.app.pruebatecnicaback.repository.RolRepository;
import com.app.pruebatecnicaback.repository.UsersRepository;
import com.app.pruebatecnicaback.service.UsersService;
import com.app.pruebatecnicaback.util.ParamsApp;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;


@Transactional
@Service
public class UsersServiceImp implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private RolRepository rolRepository;
    @Override
    public List<UsersDTO> findAllUsers() {
        return (List<UsersDTO>) usersRepository.findAll().stream().map(UsersMapping::getUsersDTOFromUsers).collect(Collectors.toList());
    }

    @Override
    public String createUser(UsersDTO usersDTO, String typeUser) {
        if(typeUser.equals(ParamsApp.ROLE_ADMIN)) {
            Rol getRolUserAdmin = rolRepository.findByRolName(ParamsApp.ROLE_ADMIN).get();
            Users saveUser = new Users();
            saveUser.setUserId(usersDTO.getUserId());
            saveUser.setPassword(usersDTO.getPassword());
            saveUser.setUserLogin(usersDTO.getUserLogin());
            saveUser.setName(usersDTO.getName());
            saveUser.setLastName(usersDTO.getLastName());
            saveUser.setEmail(usersDTO.getEmail());
            saveUser.getRols().add(getRolUserAdmin);
            usersRepository.save(saveUser);
            return "Se ha Creado Correctamente el usuario Admin";
        } else {
            Rol getRolUserAdmin = rolRepository.findByRolName(ParamsApp.ROLE_USER).get();
            Users saveUser = new Users();
            saveUser.setUserId(usersDTO.getUserId());
            saveUser.setPassword(usersDTO.getPassword());
            saveUser.setUserLogin(usersDTO.getUserLogin());
            saveUser.setName(usersDTO.getName());
            saveUser.setLastName(usersDTO.getLastName());
            saveUser.setEmail(usersDTO.getEmail());
            saveUser.getRols().add(getRolUserAdmin);
            usersRepository.save(saveUser);
            return "Se ha Creado Correctamente el usuario";
        }

    }





}
