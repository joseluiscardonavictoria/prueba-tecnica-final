package com.app.pruebatecnicaback.serviceimp;

import com.app.pruebatecnicaback.dto.EmployeeDTO;
import com.app.pruebatecnicaback.dto.ListEmployeDTO;
import com.app.pruebatecnicaback.entity.Employees;
import com.app.pruebatecnicaback.mapping.ListEmployeeMapping;
import com.app.pruebatecnicaback.repository.EmployeeRepository;
import com.app.pruebatecnicaback.repository.ParametersRepository;
import com.app.pruebatecnicaback.service.EmployeeService;
import com.app.pruebatecnicaback.util.ParamsApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class EmployeeServiceImp implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ParametersRepository parametersRepository;


    private final JdbcTemplate jdbcTemplate;

    public EmployeeServiceImp(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<ListEmployeDTO> listEmployees() {
        return (List<ListEmployeDTO>) employeeRepository.findAll().stream().map(ListEmployeeMapping::getListEmployeDTOFromEployees).collect(Collectors.toList());
    }

    @Override
    public String saveEmployee(EmployeeDTO employeeDTO) {
        Employees saveEmployees = new Employees();
        saveEmployees.setAddres(employeeDTO.getAddres());
        saveEmployees.setCountry(employeeDTO.getCountry());
        saveEmployees.setDeparment(employeeDTO.getDeparment());
        saveEmployees.setEmail(createEmail(employeeDTO.getName().replace(" ",""), employeeDTO.getFirstSurname().replace(" ",""), employeeDTO.getDomainCountry()));
        saveEmployees.setName(employeeDTO.getName());
        saveEmployees.setGender(employeeDTO.getGender());
        saveEmployees.setFirstSurname(employeeDTO.getFirstSurname());
        saveEmployees.setOtherName(employeeDTO.getOtherName());
        saveEmployees.setSecondSurname(employeeDTO.getSecondSurname());
        saveEmployees.setDateCreation(LocalDateTime.now());
        saveEmployees.setDomainCountry(employeeDTO.getDomainCountry());
        saveEmployees.setUserCreation(employeeDTO.getUserCreation());
        employeeRepository.save(saveEmployees);
        return "Se ha registrado correctamente el empleado";
    }

    @Override
    public String deleteEmployee(Long id) {
        employeeRepository.deleteById(id);
        return "Se ha eliminado el empleado";
    }

    @Override
    public Page<ListEmployeDTO> getAllPagination(Pageable pageable) {
        return (Page<ListEmployeDTO>) employeeRepository.findAll(pageable).map(ListEmployeeMapping::getListEmployeDTOFromEployees);
    }

    @Override
    public Page<ListEmployeDTO> searchNameEmployee(String name, Pageable pageable) {
        return (Page<ListEmployeDTO>) employeeRepository.findByName(name, pageable).map(ListEmployeeMapping::getListEmployeDTOFromEployees);
    }

    private String createEmail(String name, String firstSurname, String domainCountry) {
        String domain = parametersRepository.findByNameParameter(ParamsApp.DOMAIN).getValue().toLowerCase();
        String getName = name.toLowerCase().replace(" ","");
        String getFirstSurname = firstSurname.toLowerCase().replace(" ","");
        String getDomainCountry = domainCountry.toLowerCase();
        String email = getName + "." + getFirstSurname + domain + getDomainCountry;
        Integer count = 1;
        do {
            email = getName + "." + getFirstSurname + "." + count + domain + getDomainCountry;
            count++;
        } while (emailExists(email));

        return email;
    }

    private boolean emailExists(String email) {
        String sql = "SELECT COUNT(*) FROM tb_employees WHERE email_employee = ?";
        int count = jdbcTemplate.queryForObject(sql, new Object[] { email }, Integer.class);
        return count > 0;
    }
}
