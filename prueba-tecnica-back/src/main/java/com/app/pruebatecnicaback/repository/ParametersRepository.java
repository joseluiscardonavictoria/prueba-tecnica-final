package com.app.pruebatecnicaback.repository;

import com.app.pruebatecnicaback.entity.Parameters;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ParametersRepository extends JpaRepository<Parameters, Long> {
    public Parameters findByNameParameter(String nameParameter);



}
