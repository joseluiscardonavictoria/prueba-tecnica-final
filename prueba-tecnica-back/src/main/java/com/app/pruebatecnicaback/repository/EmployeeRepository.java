package com.app.pruebatecnicaback.repository;

import com.app.pruebatecnicaback.entity.Employees;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employees, Long> {

    public Page<Employees> findByName (String name, Pageable pageable);

    public Optional<Employees> findByEmail(String email);
}
