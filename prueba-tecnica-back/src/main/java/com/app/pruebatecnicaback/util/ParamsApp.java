package com.app.pruebatecnicaback.util;

public class ParamsApp {
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";

    public static final String OK = "Correcto";

    public static final String PROBLEM_SERVER = "Ha ocurrido un problema con el servidor";

    public static final String DOMAIN = "DOMINIO";

}
