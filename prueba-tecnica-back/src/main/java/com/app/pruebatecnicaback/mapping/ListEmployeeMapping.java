package com.app.pruebatecnicaback.mapping;

import com.app.pruebatecnicaback.dto.ListEmployeDTO;
import com.app.pruebatecnicaback.entity.Employees;

public class ListEmployeeMapping {

    public static ListEmployeDTO getListEmployeDTOFromEployees(Employees employees) {
        if(employees == null) return null;
        ListEmployeDTO setlistEmployeDTO = new ListEmployeDTO();
        setlistEmployeDTO.setEmployeeId(employees.getEmployeeId());
        setlistEmployeDTO.setAddres(employees.getAddres());
        setlistEmployeDTO.setCountry(employees.getCountry());
        setlistEmployeDTO.setDeparment(employees.getDeparment());
        setlistEmployeDTO.setEmail(employees.getEmail());
        setlistEmployeDTO.setName(employees.getName());
        setlistEmployeDTO.setGender(employees.getGender());
        setlistEmployeDTO.setFirstSurname(employees.getFirstSurname());
        setlistEmployeDTO.setOtherName(employees.getOtherName());
        setlistEmployeDTO.setSecondSurname(employees.getSecondSurname());
        return setlistEmployeDTO;
    }

    public static Employees getListEmployeDTOFromEployees(ListEmployeDTO employeeDto) {
        if(employeeDto == null) return null;
        Employees setlistEmploye = new Employees();
        setlistEmploye.setEmployeeId(employeeDto.getEmployeeId());
        setlistEmploye.setAddres(employeeDto.getAddres());
        setlistEmploye.setCountry(employeeDto.getCountry());
        setlistEmploye.setDeparment(employeeDto.getDeparment());
        setlistEmploye.setEmail(employeeDto.getEmail());
        setlistEmploye.setName(employeeDto.getName());
        setlistEmploye.setGender(employeeDto.getGender());
        setlistEmploye.setFirstSurname(employeeDto.getFirstSurname());
        setlistEmploye.setOtherName(employeeDto.getOtherName());
        setlistEmploye.setSecondSurname(employeeDto.getSecondSurname());
        return setlistEmploye;
    }
}
