package com.app.pruebatecnicaback.mapping;

import com.app.pruebatecnicaback.dto.UsersDTO;
import com.app.pruebatecnicaback.entity.Users;

public class UsersMapping {

    public static UsersDTO getUsersDTOFromUsers(Users users) {
        if(users == null) return null;
        UsersDTO setUserDto = new UsersDTO();
        setUserDto.setUserId(users.getUserId());
        setUserDto.setUserLogin(users.getUserLogin());
        setUserDto.setEmail(users.getEmail());
        setUserDto.setName(users.getName());
        setUserDto.setPassword(users.getPassword());
        setUserDto.setRols(users.getRols());
        setUserDto.setLastName(users.getLastName());
        return setUserDto;
    }

    public static Users getUsersFromUsersDTO(UsersDTO usersDTO) {
        if(usersDTO == null) return null;
        Users setUser = new Users();
        setUser.setUserId(usersDTO.getUserId());
        setUser.setUserLogin(usersDTO.getUserLogin());
        setUser.setEmail(usersDTO.getEmail());
        setUser.setName(usersDTO.getName());
        setUser.setPassword(usersDTO.getPassword());
        setUser.setRols(usersDTO.getRols());
        setUser.setLastName(usersDTO.getLastName());
        return setUser;
    }
}
