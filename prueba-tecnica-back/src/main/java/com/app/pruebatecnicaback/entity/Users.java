package com.app.pruebatecnicaback.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "TB_USERS")
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_USER", length = 30)
    private Long userId;

    @Column(name = "USER_NAME", length = 30)
    private String name;

    @Column(name = "USER_LAST_NAME", length = 60)
    private String lastName;

    @Column(name = "USER_LOGIN", unique = true)
    private String userLogin;

    @Column(name = "USER_EMAIL", unique = true)
    private String email;

    @Column(name = "USER_PASSWORD")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="TB_USERS_ROLS", joinColumns = @JoinColumn(name = "ID_USER"),
            inverseJoinColumns = @JoinColumn(name = "ID_ROL"),
            uniqueConstraints = {@UniqueConstraint(columnNames = {"ID_USER","ID_ROL"})})
    private Set<Rol> rols = new HashSet<>();

    public Users(Long userId, String name, String lastName, String userLogin, String email, String password, Set<Rol> rols) {
        this.userId = userId;
        this.name = name;
        this.lastName = lastName;
        this.userLogin = userLogin;
        this.email = email;
        this.password = password;
        this.rols = rols;
    }

    public Users() {

    }



    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Rol> getRols() {
        return rols;
    }

    public void setRols(Set<Rol> rols) {
        this.rols = rols;
    }
}
