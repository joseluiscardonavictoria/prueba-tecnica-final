package com.app.pruebatecnicaback.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table( name = "TB_PARAMETERS")
public class Parameters implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PARAMETER_ID")
    private Long parameterId;

    @Column(name = "NAME_PARAMETER")
    private String nameParameter;

    @Column(name = "VALUE_PARAMETER")
    private String value;

    @Column(name = "DATE_CREATION_PARAMETER")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING)
    protected LocalDateTime dateCreation;


    public Parameters(Long parameterId, String nameParameter, String value, LocalDateTime dateCreation) {
        this.parameterId = parameterId;
        this.nameParameter = nameParameter;
        this.value = value;
        this.dateCreation = dateCreation;
    }

    public Parameters() {

    }

    public Long getParameterId() {
        return parameterId;
    }

    public void setParameterId(Long parameterId) {
        this.parameterId = parameterId;
    }

    public String getNameParameter() {
        return nameParameter;
    }

    public void setNameParameter(String nameParameter) {
        this.nameParameter = nameParameter;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }
}
