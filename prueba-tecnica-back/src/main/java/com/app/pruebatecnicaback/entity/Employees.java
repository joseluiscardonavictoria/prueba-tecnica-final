package com.app.pruebatecnicaback.entity;


import com.app.pruebatecnicaback.enums.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table( name = "TB_EMPLOYEES")
public class Employees {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EMPLOYEE_ID")
    private Long employeeId;

    @Column(name = "NAME_EMPLOYEE")
    private String name;

    @Column(name = "OTHERNAME_EMPLOYEE")
    private String otherName;

    @Column(name = "FIRSTSURNAME_EMPLOYEE")
    private String firstSurname;

    @Column(name = "SECONDSURNAME_EMPLOYEE")
    private String secondSurname;


    @Column(name = "EMAIL_EMPLOYEE")
    private String email;

    @Column(name = "COUNTRY_EMPLOYEE")
    private String country;

    @Column(name = "DEPARMENT_EMPLOYEE")
    private String deparment;

    @Column(name = "ADDRESS_EMPLOYEE")
    private String addres;

    @Column(name = "GENDER_EMPLOYEE")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "DOMAINCOUNTRY_EMPLOYEE")
    private String domainCountry;

    @Column(name = "DATE_CREATION_PARAMETER")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING)
    protected LocalDateTime dateCreation;

    @Column(name = "USER_CREATION")
    private String userCreation;

    public Employees(Long employeeId, String name, String otherName, String firstSurname, String secondSurname, String email, String country, String deparment, String addres, Gender gender, String domainCountry, LocalDateTime dateCreation, String userCreation) {
        this.employeeId = employeeId;
        this.name = name;
        this.otherName = otherName;
        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.email = email;
        this.country = country;
        this.deparment = deparment;
        this.addres = addres;
        this.gender = gender;
        this.domainCountry = domainCountry;
        this.dateCreation = dateCreation;
        this.userCreation = userCreation;
    }

    public Employees () {

    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDeparment() {
        return deparment;
    }

    public void setDeparment(String deparment) {
        this.deparment = deparment;
    }

    public String getAddres() {
        return addres;
    }

    public void setAddres(String addres) {
        this.addres = addres;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDateTime getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(LocalDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(String userCreation) {
        this.userCreation = userCreation;
    }

    public String getDomainCountry() {
        return domainCountry;
    }

    public void setDomainCountry(String domainCountry) {
        this.domainCountry = domainCountry;
    }
}
