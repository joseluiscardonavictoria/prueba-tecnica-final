package com.app.pruebatecnicaback.controller.api;

import com.app.pruebatecnicaback.dto.UsersDTO;
import com.app.pruebatecnicaback.util.ResponseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface UsersApi {

    @GetMapping("/get-all-users")
    public ResponseEntity<ResponseService> getAllClients();

    @PostMapping("/save-user")
    public ResponseEntity<ResponseService> saveUser(@RequestBody UsersDTO user, @RequestParam(value = "typeUser") String typeUser);



}
