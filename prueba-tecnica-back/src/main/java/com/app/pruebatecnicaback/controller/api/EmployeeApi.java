package com.app.pruebatecnicaback.controller.api;

import com.app.pruebatecnicaback.dto.EmployeeDTO;
import com.app.pruebatecnicaback.dto.ListEmployeDTO;
import com.app.pruebatecnicaback.dto.UsersDTO;
import com.app.pruebatecnicaback.util.ResponseService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface EmployeeApi {

    @GetMapping("/get-all-employees")
    public ResponseEntity<ResponseService> getAllEmployees();


    @GetMapping("/get-all-employees-page/{page}")
    public ResponseEntity<ResponseService> getAllEmployeePage(@PathVariable Integer page);


    @GetMapping("/search-name-employees/{employee}/{page}")
    public ResponseEntity<ResponseService> searchEmployeeName(@PathVariable String employee, @PathVariable Integer page);

    @PostMapping("/save-employee")
    public ResponseEntity<ResponseService> saveEmployee(@RequestBody EmployeeDTO employeeDTO);


    @DeleteMapping("/delete-employee/{employee}")
    public ResponseEntity<ResponseService> deleteEmployee(@PathVariable Long employee);
}
