package com.app.pruebatecnicaback.controller.rest;


import com.app.pruebatecnicaback.controller.api.EmployeeApi;
import com.app.pruebatecnicaback.dto.EmployeeDTO;
import com.app.pruebatecnicaback.dto.ListEmployeDTO;
import com.app.pruebatecnicaback.dto.UsersDTO;
import com.app.pruebatecnicaback.service.EmployeeService;
import com.app.pruebatecnicaback.util.ParamsApp;
import com.app.pruebatecnicaback.util.ResponseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.LogManager;

@CrossOrigin("*")
@RestController
@RequestMapping({"/v1/api" })
public class EmployeeController implements EmployeeApi {

    private final Logger logger = LoggerFactory.getLogger(EmployeeController.class);


    @Autowired
    private EmployeeService employeeService;


    @Override
    public ResponseEntity<ResponseService> getAllEmployees() {
        ResponseService responseService = new ResponseService();
        try {
            responseService.setTimeStamp(LocalDateTime.now());
            Map<String, List<ListEmployeDTO>> data = new HashMap<>();
            data.put("server", employeeService.listEmployees());
            responseService.setData(data);
            responseService.setStatus(HttpStatus.OK);
            responseService.setStatusCode(HttpStatus.OK.value());
            responseService.setMessage(ParamsApp.OK);
            return ResponseEntity.status(200).body(responseService);
        } catch (Exception e) {
            responseService.setTimeStamp(LocalDateTime.now());
            responseService.setStatus(HttpStatus.BAD_REQUEST);
            responseService.setStatusCode(HttpStatus.BAD_REQUEST.value());
            responseService.setMessage(ParamsApp.PROBLEM_SERVER);
            logger.info(e.getMessage().toString());
            return ResponseEntity.status(400).body(responseService);
        }
    }

    @Override
    public ResponseEntity<ResponseService> getAllEmployeePage(Integer page) {
        ResponseService responseService = new ResponseService();
        try {
            responseService.setTimeStamp(LocalDateTime.now());
            Map<String, Page<ListEmployeDTO>> data = new HashMap<>();
            Pageable pageable = PageRequest.of( page,10);
            data.put("server", employeeService.getAllPagination(pageable));
            responseService.setData(data);
            responseService.setStatus(HttpStatus.OK);
            responseService.setStatusCode(HttpStatus.OK.value());
            responseService.setMessage(ParamsApp.OK);
            return ResponseEntity.status(200).body(responseService);
        } catch (Exception e) {
            responseService.setTimeStamp(LocalDateTime.now());
            responseService.setStatus(HttpStatus.BAD_REQUEST);
            responseService.setStatusCode(HttpStatus.BAD_REQUEST.value());
            responseService.setMessage(ParamsApp.PROBLEM_SERVER);
            logger.info(e.getMessage());
            return ResponseEntity.status(400).body(responseService);
        }
    }

    @Override
    public ResponseEntity<ResponseService> searchEmployeeName(String employee, Integer page) {
        ResponseService responseService = new ResponseService();
        try {
            responseService.setTimeStamp(LocalDateTime.now());
            Map<String, Page<ListEmployeDTO>> data = new HashMap<>();
            Pageable pageable = PageRequest.of( page,10);
            data.put("server", employeeService.searchNameEmployee(employee, pageable));
            responseService.setData(data);
            responseService.setStatus(HttpStatus.OK);
            responseService.setStatusCode(HttpStatus.OK.value());
            responseService.setMessage(ParamsApp.OK);
            return ResponseEntity.status(200).body(responseService);
        } catch (Exception e) {
            responseService.setTimeStamp(LocalDateTime.now());
            responseService.setStatus(HttpStatus.BAD_REQUEST);
            responseService.setStatusCode(HttpStatus.BAD_REQUEST.value());
            responseService.setMessage(ParamsApp.PROBLEM_SERVER);
            logger.info(e.getMessage());
            return ResponseEntity.status(400).body(responseService);
        }
    }


    @Override
    public ResponseEntity<ResponseService> saveEmployee(EmployeeDTO employeeDTO) {
        ResponseService responseService = new ResponseService();
        try {
            responseService.setTimeStamp(LocalDateTime.now());
            Map<String, String> data = new HashMap<>();
            data.put("server", employeeService.saveEmployee(employeeDTO));
            responseService.setData(data);
            responseService.setStatus(HttpStatus.OK);
            responseService.setStatusCode(HttpStatus.OK.value());
            responseService.setMessage(ParamsApp.OK);
            return ResponseEntity.status(200).body(responseService);
        } catch (Exception e) {
            responseService.setTimeStamp(LocalDateTime.now());
            responseService.setStatus(HttpStatus.BAD_REQUEST);
            responseService.setStatusCode(HttpStatus.BAD_REQUEST.value());
            responseService.setMessage(ParamsApp.PROBLEM_SERVER);
            logger.info(e.getMessage());
            return ResponseEntity.status(400).body(responseService);
        }
    }

    @Override
    public ResponseEntity<ResponseService> deleteEmployee(Long employee) {
        ResponseService responseService = new ResponseService();
        try {
            responseService.setTimeStamp(LocalDateTime.now());
            Map<String, String> data = new HashMap<>();
            employeeService.deleteEmployee(employee);
            data.put("server", "Se ha eliminado correctamente el empleado");
            responseService.setData(data);
            responseService.setStatus(HttpStatus.OK);
            responseService.setStatusCode(HttpStatus.OK.value());
            responseService.setMessage(ParamsApp.OK);
            return ResponseEntity.status(200).body(responseService);
        } catch (Exception e) {
            responseService.setTimeStamp(LocalDateTime.now());
            responseService.setStatus(HttpStatus.BAD_REQUEST);
            responseService.setStatusCode(HttpStatus.BAD_REQUEST.value());
            responseService.setMessage(ParamsApp.PROBLEM_SERVER);
            logger.info(e.getMessage());
            return ResponseEntity.status(400).body(responseService);
        }
    }
}
