package com.app.pruebatecnicaback.controller.rest;


import com.app.pruebatecnicaback.controller.api.UsersApi;
import com.app.pruebatecnicaback.dto.UsersDTO;
import com.app.pruebatecnicaback.service.UsersService;
import com.app.pruebatecnicaback.util.ParamsApp;
import com.app.pruebatecnicaback.util.ResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping({"/v1/api" })
public class UsersController implements UsersApi {

    @Autowired
    private UsersService usersService;

    @Override
    public ResponseEntity<ResponseService> getAllClients() {

        ResponseService responseService = new ResponseService();
        try {
            responseService.setTimeStamp(LocalDateTime.now());
            Map<String, List<UsersDTO>> data = new HashMap<>();
            data.put("server", usersService.findAllUsers());
            responseService.setData(data);
            responseService.setStatus(HttpStatus.OK);
            responseService.setStatusCode(HttpStatus.OK.value());
            responseService.setMessage(ParamsApp.OK);
            return ResponseEntity.status(200).body(responseService);
        } catch (Exception e) {
            responseService.setTimeStamp(LocalDateTime.now());
            responseService.setStatus(HttpStatus.BAD_REQUEST);
            responseService.setStatusCode(HttpStatus.BAD_REQUEST.value());
            responseService.setMessage(ParamsApp.PROBLEM_SERVER);
            return ResponseEntity.status(400).body(responseService);
        }
    }

    @Override
    public ResponseEntity<ResponseService> saveUser(UsersDTO user, String typeUser) {
        ResponseService responseService = new ResponseService();
        try {
            responseService.setTimeStamp(LocalDateTime.now());
            Map<String, String> data = new HashMap<>();
            data.put("server", usersService.createUser(user, typeUser));
            responseService.setData(data);
            responseService.setStatus(HttpStatus.OK);
            responseService.setStatusCode(HttpStatus.OK.value());
            responseService.setMessage(ParamsApp.OK);
            return ResponseEntity.status(200).body(responseService);
        } catch (Exception e) {
            responseService.setTimeStamp(LocalDateTime.now());
            responseService.setStatus(HttpStatus.BAD_REQUEST);
            responseService.setStatusCode(HttpStatus.BAD_REQUEST.value());
            responseService.setMessage(ParamsApp.PROBLEM_SERVER);
            return ResponseEntity.status(400).body(responseService);
        }
    }
}
