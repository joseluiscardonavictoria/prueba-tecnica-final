package com.app.pruebatecnicaback.service;

import com.app.pruebatecnicaback.dto.UsersDTO;

import java.util.List;

public interface UsersService {

    public List<UsersDTO> findAllUsers();

    public String createUser(UsersDTO usersDTO, String typeUser);

}
