package com.app.pruebatecnicaback.service;

import com.app.pruebatecnicaback.dto.EmployeeDTO;
import com.app.pruebatecnicaback.dto.ListEmployeDTO;
import javafx.scene.control.Pagination;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EmployeeService {

    public List<ListEmployeDTO> listEmployees();

    public String saveEmployee(EmployeeDTO employeeDTO);

    public String deleteEmployee(Long id);

    public Page<ListEmployeDTO> getAllPagination(Pageable pageable);

    public Page<ListEmployeDTO> searchNameEmployee(String name, Pageable pageable);
}
