import { environment } from './../../../enviroment/enviroment';
import { Observable, map } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private _http: HttpClient) { }

  public listCountrys(

  ): Observable<any[] | any> {
    return this._http
      .get<any[] | any>(
        `${environment.urlCountry}`
      );
  }
}
