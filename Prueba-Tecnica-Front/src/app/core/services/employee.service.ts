import { Employee } from './../models/employee';
import { environment } from './../../../enviroment/enviroment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, tap } from 'rxjs';
import { ListEmployeeResponse } from '../models/list-employee';

@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  public optionsEmployee: string[] = [];

  constructor(private _http: HttpClient) {}

  public listEmployeeData(
    page: number
  ): Observable<ListEmployeeResponse[] | any | ListEmployeeResponse> {
    return this._http
      .get<ListEmployeeResponse[] | any | ListEmployeeResponse>(
        `${environment.url}get-all-employees-page/${page}`
      )
      .pipe(map((res) => res?.data?.server));
  }

  public searchNameEmployee(
    name: string | any,
    page: number
  ): Observable<ListEmployeeResponse[] | any | ListEmployeeResponse> {
    return this._http
      .get<ListEmployeeResponse[] | any | ListEmployeeResponse>(
        `${environment.url}search-name-employees/${name}/${page}`
      )
      .pipe(map((res) => res?.data?.server));
  }

  public deleteEmployee(
    employee: number
  ): Observable<ListEmployeeResponse[] | any | ListEmployeeResponse> {
    return this._http
      .delete<ListEmployeeResponse[] | any | ListEmployeeResponse>(
        `${environment.url}delete-employee/${employee}`
      )
      .pipe(map((res) => res?.data?.server));
  }

  public getAutocompletEmployees(): Observable<
    ListEmployeeResponse[] | any | ListEmployeeResponse
  > {
    return this.optionsEmployee.length
      ? of(this.optionsEmployee)
      : this._http
          .get<ListEmployeeResponse[] | any | ListEmployeeResponse>(
            `${environment.url}get-all-employees`
          )
          .pipe(tap((data) => (this.optionsEmployee = data)));
  }

  public saveEmployee(employee: Employee): Observable<Employee | any> {
    return this._http.post<Employee | any>(`${environment.url}save-employee`, employee).pipe(map((res) => res?.data?.server));
  }
}
