import { IResponseService } from './../interfaces/response-service';
export class Employee {
  name?: string;
  otherName?: string;
  firstSurname?: string;
  secondSurname?: string;
  country?: string;
  deparment?: string;
  addres?: string;
  gender?: string;
  domainCountry?: string;
}

export interface ListEmployeeResponse extends IResponseService {
  timeStamp: Date | any | number;
  statusCode: number;
  status: string | any;
  reason: string;
  message: string | any;
  developerMessage: string | any;
  data: Employee | any | string;
}
