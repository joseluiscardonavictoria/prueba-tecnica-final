
import { Gender } from '../enum/gender';
import { IResponseService } from '../interfaces/response-service';

export class ListEmployee {
    employeeId?: string;
    name?: string;
    otherName?: string;
    firstSurname?: string;
    secondSurname?: string;
    email?: string;
    country?: string;
    deparment?: string;
    addres?: string;
    gender?: string | any | Gender;
}

export interface ListEmployeeResponse extends IResponseService {
  timeStamp: Date | any | number;
  statusCode: number;
  status: string | any;
  reason: string;
  message: string | any;
  developerMessage: string | any;
  data: ListEmployee[] | ListEmployee  | any;
}
