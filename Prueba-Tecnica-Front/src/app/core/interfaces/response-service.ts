export interface IResponseService {
  timeStamp: Date | any | number;
  statusCode: number;
  status: string | any;
  reason: string;
  message: string | any;
  developerMessage: string | any;
  data: any[] | object[] | object | any;
}
