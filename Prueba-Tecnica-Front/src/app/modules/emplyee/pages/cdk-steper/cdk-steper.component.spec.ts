import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CdkSteperComponent } from './cdk-steper.component';

describe('CdkSteperComponent', () => {
  let component: CdkSteperComponent;
  let fixture: ComponentFixture<CdkSteperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CdkSteperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CdkSteperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
