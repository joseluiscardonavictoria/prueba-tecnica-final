import { CdkStepper } from '@angular/cdk/stepper';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cdk-steper',
  templateUrl: './cdk-steper.component.html',
  styleUrls: ['./cdk-steper.component.less'],
  providers: [{ provide: CdkStepper, useExisting: CdkSteperComponent }]
})
export class CdkSteperComponent extends CdkStepper implements OnInit {

  public progress!: number;

  ngOnInit(): void {

    setTimeout(() => {
      this.progress = 100 / this.steps.length;
    }, 100);
  }

  selectStepByIndex(index: number): void {
    this.selectedIndex = index;
  }

  backStep() {
    this.progress -= 100 / this.steps.length;
    console.log(this.progress);

  }

  nextStep() {

    if (this.selected?.stepControl?.valid) {
      this.progress +=  this.progress <100 ? 100 / this.steps.length: 0;
      this.next();
    } else {
      if(this.selected?.stepControl == undefined) {
        this.progress += this.progress <100 ? 100 / this.steps.length: 0
        this.next();
      }
    }
  }

}
