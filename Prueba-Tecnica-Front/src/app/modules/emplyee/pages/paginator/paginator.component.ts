import { RouterModule, ActivatedRoute, RouterLink, Router } from '@angular/router';
import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.less']
})
export class PaginatorComponent implements OnInit, OnChanges {



  @Input() paginator: any;
  public pages: number[] = [];
  public since!: number;
  public until!: number;

  ngOnInit(): void {
    console.log('Paginator ===>', this.paginator);
    this.pages = new Array(this.paginator?.totalPages).fill(0).map((_value, indice) => indice + 1);

  }

  ngOnChanges(changes: SimpleChanges) {
    let pageUpdate = changes['paginator'];

    if (pageUpdate.previousValue) {
      this.initPaginator();
    }

  }

  constructor(private _router: Router){

  }

  public routerLink(page: number) : void {
    this._router.navigate(['/dashboard/employee-module/list-employee/page', page]);
  }


  private initPaginator(): void {
    this.since = Math.min(Math.max(1, this.paginator.number - 4), this.paginator.totalPages - 5);
    this.until = Math.max(Math.min(this.paginator.totalPages, this.paginator.number + 4), 6);

    if (this.paginator.totalPages > 5) {
      this.pages = new Array(this.since - this.until + 1).fill(0).map((_valor, indice) => indice + this.until);
    } else {
      this.pages = new Array(this.paginator.totalPages).fill(0).map((_valor, indice) => indice + 1);
    }
  }

}
