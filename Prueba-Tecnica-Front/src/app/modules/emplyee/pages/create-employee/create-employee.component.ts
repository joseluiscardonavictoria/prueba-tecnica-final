import { Employee } from './../../../../core/models/employee';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { CountryService } from './../../../../core/services/country.service';

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { Patterns } from 'src/app/utils/pattern';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.less'],
})
export class CreateEmployeeComponent implements OnInit {
  public employeePersonalForm!: FormGroup;
  public employeeContactoForm!: FormGroup;
  public listCountrys: any = [];

  ngOnInit(): void {
    this.listAllContrys();
  }

  public initForms() {
    this.employeePersonalForm = this._builder.group({
      name: [
        '',
        [Validators.required, Validators.pattern(Patterns.FIRSTSURNAME)],
      ],
      otherName: [
        '',
        [Validators.required, Validators.pattern(Patterns.OTHERNAME)],
      ],
      firstSurname: [
        '',
        [Validators.required, Validators.pattern(Patterns.FIRSTSURNAME)],
      ],
      secondSurname: ['', [Validators.required]],
      gender: ['', [Validators.required]],
    });

    this.employeeContactoForm = this._builder.group({
      country: ['', [Validators.required]],
      deparment: ['', [Validators.required]],
      addres: [''],
    });
  }

  public listAllContrys(): void {
    this._countryServices.listCountrys().subscribe((res: any) => {
      this.listCountrys = res;
      console.log(this.listCountrys);
    });
  }

  public validInputFormPersonal(field: string): boolean | null | undefined {
    return (
      this.employeePersonalForm.get(field)?.touched &&
      this.employeePersonalForm.get(field)?.hasError('required')
    );
  }

  public validInputFormContact(field: string): boolean | null | undefined {
    return (
      this.employeeContactoForm.get(field)?.touched &&
      this.employeeContactoForm.get(field)?.hasError('required')
    );
  }

  public validPatter(field: string): boolean | null | undefined {
    return (
      this.employeePersonalForm.get(field)?.touched &&
      this.employeePersonalForm.get(field)?.hasError('pattern')
    );
  }

  public saveEmployee(): void {
    if (this.employeePersonalForm.invalid) {
      return Object.values(this.employeePersonalForm.controls).forEach(
        (validFields) => {
          validFields.markAllAsTouched();
        }
      );
    }

    if (this.employeeContactoForm.invalid) {
      return Object.values(this.employeeContactoForm.controls).forEach(
        (validFields) => {
          validFields.markAllAsTouched();
        }
      );
    }

    const getValueCountry = this.employeeContactoForm.get('country')?.value;
    const formSaveEmployee: Employee = {
      name: this.employeePersonalForm.get('name')?.value,
      otherName: this.employeePersonalForm.get('otherName')?.value,
      firstSurname: this.employeePersonalForm.get('firstSurname')?.value,
      secondSurname: this.employeePersonalForm.get('secondSurname')?.value,
      country: getValueCountry.name?.common,
      deparment: this.employeeContactoForm.get('deparment')?.value,
      addres: this.employeeContactoForm.get('addres')?.value,
      gender: this.employeePersonalForm.get('gender')?.value,
      domainCountry: getValueCountry.tld[0],
    };
    this._employeeService.saveEmployee(formSaveEmployee).subscribe(
      (res) => {
        this._dialogRef.close();
        Swal.fire({
          title: 'Empleado Creado',
          text: `${res}`,
          icon: 'success',
          confirmButtonText: 'Ok',
          confirmButtonColor: '#1c2934',
        });
      },
      (err) => {
        console.warn(err);
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          confirmButtonColor: '#1c2934',
          text: 'Ha ocurrido un error, intente nuevamente',
        });
      }
    );
  }

  constructor(
    private _countryServices: CountryService,
    private _dialogRef: MatDialogRef<CreateEmployeeComponent>,
    private _builder: FormBuilder,
    private _employeeService: EmployeeService
  ) {
    this.initForms();
  }
}
