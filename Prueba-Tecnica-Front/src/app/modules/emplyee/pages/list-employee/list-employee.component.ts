import { CreateEmployeeComponent } from './../create-employee/create-employee.component';
import { ActivatedRoute } from '@angular/router';
import {
  Observable,
  map,
  startWith,
  debounceTime,
  distinctUntilChanged,
  switchMap,
} from 'rxjs';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { EmployeeService } from 'src/app/core/services/employee.service';
import { ListEmployee } from 'src/app/core/models/list-employee';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import Swal from 'sweetalert2';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.less'],
})
export class ListEmployeeComponent implements OnInit {
  public controlSearch = new FormControl();
  public filteredOptions!: Observable<ListEmployee[] | any>;
  public employees: ListEmployee[] | any = [];
  public setPaginator!: any;
  ngOnInit(): void {
    this.getAllEmployees();
    this.validIsNoNull();
    this.filterQuerySearch();


  }

  constructor(private _employeeService: EmployeeService,
    private _activeRoute: ActivatedRoute, private _dialog: MatDialog) {

  }



  private getAllEmployees() {
    this._activeRoute.paramMap.subscribe((params: any) => {
      let page: number = +params.get('page');
      if (!page) page = 0;
      this._employeeService.listEmployeeData(page).subscribe((res: any) => {
        this.employees = res.content;
        this.setPaginator = res;
        console.log('RES', this.setPaginator);
      });
    });
  }

  public filterQuerySearch(): void {
    this.filteredOptions = this.controlSearch.valueChanges.pipe(
      startWith(''),
      debounceTime(400),
      distinctUntilChanged(),
      switchMap((val) => {
        return this.filter(val || '');
      })
    );
  }


  public addEmployee(): void {
    let dialogRef!: MatDialogRef<CreateEmployeeComponent, any>;
    dialogRef = this._dialog.open(CreateEmployeeComponent, {
      panelClass: 'dialog-responsive-user',
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
      this.getAllEmployees();
    });
  }

  public validIsNoNull(): void {
    this.controlSearch.valueChanges.subscribe((res) => {
      if(!res) this.getAllEmployees();
    });
  }

  public showNameProduct(
    employee?: ListEmployee | any
  ): string | undefined | any {
    return employee ? employee.name : '';
  }

  private filter(val: string): Observable<ListEmployee[] | any> {
    return this._employeeService.getAutocompletEmployees().pipe(
      map(({ data }) =>
        data.server.filter((option: any) => {
          return (
            option.name.toLowerCase().indexOf(val.toLowerCase()) === 0 ||
            option.firstSurname.toLowerCase().indexOf(val.toLowerCase()) === 0
          );
        })
      )
    );
  }

  public selectName(event: MatAutocompleteSelectedEvent): void  {
    const getEmployee = event.option.value as ListEmployee;
    this._activeRoute.paramMap.subscribe((params: any) => {
      let page: number = +params.get('page');
      if (!page) page = 0;
      this._employeeService.searchNameEmployee(getEmployee.name, page).subscribe((res: any) => {
        this.employees = res.content;
        this.setPaginator = res;
      });
    });
  }


  public deleteProduct(id: number): void {
    Swal.fire({
      title: '¿Eliminar empleado?',
      text: '¿Deseas eliminar este empleado?',
      icon: 'info',
      showCancelButton: true,
      buttonsStyling: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Ok',
      confirmButtonColor: 'red',
      cancelButtonColor: '#282828',
    }).then((result) => {
      if (result.isConfirmed) {
        this._employeeService.deleteEmployee(id).subscribe(
          (res: any) => {
            this.employees = this.employees.filter(
              (item: any) => item.employeeId !== id
            );
          },
          (err) => {
            console.warn(err);
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              confirmButtonColor: '#282828',
              text: 'Ha ocurrido un error, intente nuevamente o comuniquese con el administrador.',
            });
          }
        );
        Swal.fire('Eliminado!', ``, 'success');
      }
    });
  }
}
