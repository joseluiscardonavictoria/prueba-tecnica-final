import { MainEmployeeComponent } from './pages/main-employee/main-employee.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListEmployeeComponent } from './pages/list-employee/list-employee.component';
import { CreateEmployeeComponent } from './pages/create-employee/create-employee.component';

const routes: Routes = [
  {
    path: '',
    component: MainEmployeeComponent,
    children: [
      {
        path: 'list-employee',
        component: ListEmployeeComponent
      },
      {
        path: 'list-employee/page/:page',
        component: ListEmployeeComponent
      },
      {
        path: 'create-employee',
        component: CreateEmployeeComponent
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmplyeeRoutingModule { }
