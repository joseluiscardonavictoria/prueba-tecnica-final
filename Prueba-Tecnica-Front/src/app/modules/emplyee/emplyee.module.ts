import { MainModule } from 'src/app/shared/main-module/main/main.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmplyeeRoutingModule } from './emplyee-routing.module';
import { ListEmployeeComponent } from './pages/list-employee/list-employee.component';
import { MainEmployeeComponent } from './pages/main-employee/main-employee.component';
import { PaginatorComponent } from './pages/paginator/paginator.component';
import { CreateEmployeeComponent } from './pages/create-employee/create-employee.component';
import { CdkSteperComponent } from './pages/cdk-steper/cdk-steper.component';


@NgModule({
  declarations: [
    ListEmployeeComponent,
    MainEmployeeComponent,
    PaginatorComponent,
    CreateEmployeeComponent,
    CdkSteperComponent
  ],
  imports: [
    CommonModule,
    EmplyeeRoutingModule,
    MainModule
  ]
})
export class EmplyeeModule { }
