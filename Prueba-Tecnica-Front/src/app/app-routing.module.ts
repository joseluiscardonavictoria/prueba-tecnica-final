import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard'
  },
  {
    path: 'dashboard',
    // canActivate: [guard],
    // data: {
    //   rol: [Role.ROLE_ADMIN, Role.ROLE_USER]
    // },
    loadChildren: () =>
      import('src/app/shared/dashboard/dashboard/dashboard.module').then(
        (d) => d.DashboardModule
      )
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
