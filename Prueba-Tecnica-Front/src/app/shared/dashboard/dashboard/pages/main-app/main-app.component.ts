import { ChangeDetectorRef, Component } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-main-app',
  templateUrl: './main-app.component.html',
  styleUrls: ['./main-app.component.less']
})
export class MainAppComponent {

  public sideBarOpen: boolean = false;
  public mobileQuery!: MediaQueryList;

  public sideBarToggler(): void {

    this.sideBarOpen = !this.sideBarOpen;
  }

  private _mobileQueryListener: () => void;

  constructor(
    private _media: MediaMatcher,
    private _changeDetectorRef: ChangeDetectorRef
  ) {
    this.mobileQuery = _media.matchMedia('(max-width: 800px)');
    this._mobileQueryListener = () => _changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnInit() {
    document.title = 'Dashboard | Inicio';
  }

  ngOnDestroy(): void {}

}
