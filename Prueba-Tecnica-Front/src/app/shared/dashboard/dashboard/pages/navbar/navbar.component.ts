import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent {

  @Output() toggleSidebarForMe: EventEmitter<any> = new EventEmitter();

  public toggleSidebar() {
    this.toggleSidebarForMe.emit();
  }


}
