import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.less']
})
export class SidebarComponent  {

  @Output() toggleSidebarForMe: EventEmitter<any> = new EventEmitter();
  public panelOpenState: boolean = false;

  public step: number = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }



  public toggleSidebar() {
    this.toggleSidebarForMe.emit();
  }



}
