export class Patterns {
  public static readonly FIRSTSURNAME =  /^[A-Z\s]{1,20}$/;
  public static readonly OTHERNAME = /^[A-Z\s]{1,50}$/;
}
