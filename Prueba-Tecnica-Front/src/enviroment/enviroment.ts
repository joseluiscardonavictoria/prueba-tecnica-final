export const environment = {
  production: false,
  //192.168.10.24
  url: 'http://localhost:8080/v1/api/',
  urlCountry: 'https://restcountries.com/v3.1/all'
};
